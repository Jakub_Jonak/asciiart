package pl.edu.pwr.pp;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String ASCII = "@%#*+=-:. ";	// zmieniono nazwe
	public static char[] INTENSITY_2_ASCII = ASCII.toCharArray(); // dodano w celu latwiejszego odniesienia w funkcji "intensityToAscii"

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity) {
		// TODO Wasz kod
		
		if(intensity < 26) return INTENSITY_2_ASCII[0];
		else if(intensity < 52) return INTENSITY_2_ASCII[1];
		else if(intensity < 77)	return INTENSITY_2_ASCII[2];
		else if(intensity < 103) return INTENSITY_2_ASCII[3];
		else if(intensity < 128) return INTENSITY_2_ASCII[4];
		else if(intensity < 154) return INTENSITY_2_ASCII[5];
		else if(intensity < 180) return INTENSITY_2_ASCII[6];
		else if(intensity < 205) return INTENSITY_2_ASCII[7];
		else if(intensity < 231) return INTENSITY_2_ASCII[8];
		else if(intensity < 256) return INTENSITY_2_ASCII[9];
		else return 0;
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		// TODO Wasz kod
		
		int rows = intensities.length;
		int cols = intensities[0].length;	// intensitites[0] - zalozenie, ze wszystkie rozmiary rowne
		
		char[][] ascii = new char[rows][cols];	// deklaracja tablicy przechowujacej gotowy obraz ascii
		
		for(int i = 0; i < rows; ++i)
		{
			for(int j = 0; j < cols; ++j)	
			{
				ascii[i][j] = intensityToAscii(intensities[i][j]);	// konwersja na ascii
			}
		}
		
		return ascii;
	}

}
