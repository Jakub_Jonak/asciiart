package pl.edu.pwr.pp;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		// np. korzystając z java.io.PrintWriter
		// TODO Wasz kod
		
		File file = new File(fileName);
		//file.getParentFile().mkdirs();
	    
		try (PrintWriter printWriter = new PrintWriter(file)){
			
			for(int i = 0; i < ascii[0].length; ++i)
			{
				for(int j = 0; j < ascii.length; ++j)
				{
					printWriter.print(ascii[j][i]);
				}
				printWriter.print("\r\n");
			}
			printWriter.close(); 			
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}       
	}
}
